@ECHO OFF
:: %1 Path to web-root for server (used raw if no %3 is given)
:: %2 Path to git-root for git pulls.
:: %3 Roothpath added to all paths if given (ex. dropbox)
:: %4 Path for the webbroswer, relative to root

echo Starting program
echo -----

	:: Params
set documentRootPath=%1
set gitRawPath=%2
set rootPath=%3
set browserPath=%4

	:: Setup variables
set addr=127.0.0.1
set startPort=80
set atomPath=%LOCALAPPDATA%\atom\atom.exe

	:: If param %3 is given, add before documentRootPath and gitRawPath
IF NOT [%rootPath%] == [] (
	set documentPath=%rootPath%\%documentRootPath%
	set gitPath=%rootPath%\%gitRawPath%
) ELSE (
	set documentPath=%documentRootPath%
	set gitPath=%gitRawPath%
)

	:: Try to make a GIT-pull
echo Attempting a Git pull on %gitPath%
cd /D %gitPath%
git pull

echo -----

	:: Find avalible port
echo Searching for port

SET port=%startPort%

::Loop
:setPort 
netstat -o -n -a | findstr "LISTENING" | findstr ":%port%" >NUL

if %errorlevel% == 0 (
	SET /A port=port+1
	goto :setPort
)

echo Port found: %port%
echo -----

	:: Starting server
echo Starting PHP server
PHP -v

set faddr=%addr%:%port%
start call php -S %faddr% -t %documentPath%

echo Server started on %faddr% for %documentPath%
echo -----

	:: Opening webpage in chrome
start chrome %faddr%%browserPath%
echo Page %faddr%%browserPath% opened in chrome
echo -----

	:: Starts Atom if not already running
::echo Checks if Atom is running.
::tasklist|find /i "atom.exe" >NUL
::if %errorlevel% == 1  (
::	echo Starting Atom
::	start  /B atom -na %gitPath% 
::)
::echo Atom is running.
::echo -----

	:: Opens folder in Visual Studio Code
echo Opens i VS Code
code .
echo -----


	::End of program
echo Done
echo -----
PAUSE