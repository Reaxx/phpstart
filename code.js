var wshShell = new ActiveXObject("WScript.Shell");
printProjects();
//Reads the config-file and adds the projects to the FileExists
function printProjects()
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var file = fso.OpenTextFile(getCurrentDirectory()+"projects.json", 1, true);
	var jsonText = file.ReadAll();

	var parsedJson = JSON.parse(jsonText);


	var table = document.getElementById("projects");

	for (var project in parsedJson)
	{
		//Create row with cells
		let row = table.insertRow();
		let nameCell = row.insertCell(0);
		let descCell = row.insertCell(1);
		let buttonCell = row.insertCell(2);

		//Add name and description to the cells
		nameCell.innerHTML = parsedJson[project].name;
		descCell.innerHTML = parsedJson[project].desc;

		//Creates buttons
		let btn = document.createElement("button");
		let text = document.createTextNode("Start");

		//Adds document and repopath parameters
		let pathString = parsedJson[project].documentPath + ' ' + parsedJson[project].repoPath

		//add dropboxparameter
		if(parsedJson[project].useDropbox)
		{
			pathString += ' ' + getDropboxPath();
		}

		//add browersParameter
		if(typeof parsedJson[project].browserPath !== 'undefined')
		{
			pathString += ' ' + parsedJson[project].browserPath;
		}

		//Add button to the table
		btn.onclick = function() { startBat(pathString); };
		btn.appendChild(text);
		buttonCell.appendChild(btn);
	}
}


function getCurrentDirectory()
{
	var fso=new ActiveXObject("Scripting.FileSystemObject");
	path = unescape(document.location); //replace %20 with " ", etc.
	path = path.substring(8,path.lastIndexOf("/")+1); //chop off "file:///" and file name

	return path;
}

// Starts the batfile with the params from the buttons
function startBat(args)
{
	wshShell.Run(getCurrentDirectory()+"startServer.bat "+args);
}

//Finds the rootpath to the dropbox folder
function getDropboxPath()
{
	var fso = new ActiveXObject("Scripting.FileSystemObject");
	var appdata = wshShell.ExpandEnvironmentStrings("%APPDATA%");
	var localappdata = wshShell.ExpandEnvironmentStrings("%LOCALAPPDATA%");
	var jsonPath = null;

	//Finds the path to the drobpox info.json
	if (fso.FileExists(appdata+"\\Dropbox\\info.json"))
	{
		jsonPath = appdata+"\\Dropbox\\info.json";
	}
	else if (fso.FileExists(localappdata+"\\Dropbox\\info.json"))
	{
		jsonPath = localappdata+"\\Dropbox\\info.json";
	}
	else
	{
		throw "Did not find dropbox info.json";
	}

	//Reads the file
	var file = fso.OpenTextFile(jsonPath, 1, true);
	var jsonText = file.ReadAll();

	var parsedJson = JSON.parse(jsonText);

	//Returns droåbox root path
	return(parsedJson.personal.path);
}
