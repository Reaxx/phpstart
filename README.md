# PhpStart

Simple tool to quickly start working on my PHP tools (starts server, opens the page in browser and starts atom)

## Usage
Create a file named projects.json and add each project according to the following pattern: 

```
name			- String, Name to be shown in the projects-list
desc			- String, Description to be shown in the projects-list
documentPath	- String, Rootpath for the webserver (PHP develop server as default)
repoPath		- String, Path to the Git-rootfolder
browserPath		- String, Path for the webbroswer. If null, root is used. 
useDropbox		- Bool, allows the paths to be relative to the dropbox rootfolder. 

*Specials symbols need to be escaped (eg "/" should be "//")
```

```
{
	"1":
	{
		"name": "NewSimultima",
		"desc": "Prototype for a new page for simultima.se",
		"documentPath": "Repos\\Webbsidor\\newsimultima\\web",
		"repoPath": "Repos\\Webbsidor\\newsimultima",
		"useDropbox": true
	},
	"2":
	{
		"name": "NewSimultima",
		"desc": "Prototype for a new page for simultima.se",
		"documentPath": "F:\\Dropbox\\Repos\\Webbsidor\\newsimultima\\web",
		"repoPath": "F:\\Dropbox\\Repos\\Webbsidor\\newsimultima",
		"useDropbox": false
	}
}
```
